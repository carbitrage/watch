import homePage from '../pageobjects/home.page';
import { logIn } from '../helpers/auth.helper';

describe('Navbar', function () {
    it('main tabs opens', async function () {
        await homePage.open();
        const url: string = await browser.getUrl();
        await homePage.navbar.goToCharts();
        expect(browser).toHaveUrlContaining('charts');
        await homePage.navbar.goToAbout();
        expect(browser).toHaveUrlContaining('about');
        await homePage.navbar.goToHome();
        expect(browser).toHaveUrl(url);
    });

    it('profile page opens', async function () {
        await homePage.open();
        await logIn();
        await homePage.navbar.clickUserMenu();
        await homePage.navbar.chooseProfileOption();
        expect(browser).toHaveUrlContaining('profile');
    });
});