import chartsPage from '../pageobjects/charts.page';

describe('Charts', function () {
    it('pair switch changes loads new prices', async function () {
        await chartsPage.open();
        const pricesBefore: number = await chartsPage.getSumOfPrices();
        await chartsPage.changeActivePairToRandom();
        await chartsPage.waitForPricesToChange(pricesBefore);
        const pricesAfter: number = await chartsPage.getSumOfPrices();
        expect(pricesBefore).not.toEqual(pricesAfter);
    });
});