class Navbar {
    private get homeNavLink () { return $('a[data-test="nav-link"]=Home') }
    private get chartsNavLink () { return $('a[data-test="nav-link"]=Charts') }
    private get aboutNavLink () { return $('a[data-test="nav-link"]=About') }
    private get dropdownIcon () { return $('li[data-testid="userDropdown"]') }
    private get logOutOption () { return $('.dropdown-item=Log Out') }
    private get profileOption () { return $('.dropdown-item=Profile') }

    async goToHome() { (await this.homeNavLink).click() }
    async goToCharts() { (await this.chartsNavLink).click() }
    async goToAbout() { (await this.aboutNavLink).click() }

    async clickUserMenu() { (await this.dropdownIcon).click() }
    async chooseLogOutOption() { (await this.logOutOption).click() }
    async chooseProfileOption() { (await this.profileOption).click() }

    loggedIn(value: boolean) {
        expect(this.dropdownIcon).toHaveAttribute('data-status', value ? 'signed' : 'unsigned');
    }
}

export default Navbar;