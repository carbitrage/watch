import Page from "./page";
import { shuffle } from "../helpers/common.helper";

class ChartsPage extends Page {
    private get pieces () { return $$('svg > g > g > path:not([fill="none"])') }
    private get prices () { return $$('g > text[text-anchor="middle"]') }
    private get selections () { return $$('//div[contains(@id, "react-select")]') }
    private get selectedPair () { return $('//div[contains(@class, "-singleValue")]') }
    private get pairDropdownIcon () { return $('//div[h5[.="Pair"]]//div[contains(@class, "-indicatorContainer")]') }

    async open(): Promise<void> {
        await super.open('/charts');
    }

    async getAmountOfPieces(): Promise<number> {
        return (await this.pieces).length;
    }

    async getAmountOfPrices(): Promise<number> {
        return (await this.prices).length;
    }

    async waitForPairsToAppear(): Promise<void> {
        await browser.waitUntil(
            async (): Promise<boolean> => { 
                return (await this.prices).length !== 0 
            },
            {
                timeoutMsg: 'expected price values to appear'
            }
        );
    }


    async getSumOfPrices(): Promise<number> {
        await this.waitForPairsToAppear();
        const prices: WebdriverIO.ElementArray = await this.prices;
        const values: number[] = [];
        for await (const price of prices) {
            const value: number = +(await price.getText());
            values.push(value);
        }
        return values.reduce(
            function (sum: number, element: number) { 
                return sum + element;
            }, 0
        );
    }

    async changeActivePairToRandom(): Promise<void> {
        const selectedPair: string = await (await this.selectedPair).getText();
        await (await this.pairDropdownIcon).click();
        const availablePairs: WebdriverIO.ElementArray = await $$(`//div[contains(@id, "react-select") and not(contains(text(), "${selectedPair}"))]`);
        const shuffledPairs: WebdriverIO.ElementArray = await shuffle(availablePairs);
        shuffledPairs[0].click();
    }

    async waitForPricesToChange(sumBefore: number): Promise<void> {
        return new Promise(async resolve => {
            await this.waitForPairsToAppear();
            await browser.waitUntil(
                async (): Promise<boolean> => {
                    const prices = await this.getSumOfPrices();
                    return !(prices === sumBefore);
                },
                {
                    timeoutMsg: 'expected price values to change'
                }
            );
            resolve();
        });
    }
}

export default new ChartsPage;