class AuthModal {
    private get uidInput () { return $('input[aria-label="Username"]') }
    private get messageError () { return $('span[data-testid="errorMessage"]') }
    private get sendPasswordButton () { return $('button=Send Password') }
    private get passwordInput () { return $('input[aria-label="Password"]') }
    private get logInButton () { return $('button=Log In') }

    async setUserID(uid: string): Promise<void> {
        (await this.uidInput).setValue(uid);
    }

    checkForSpecificError(message: string): void {
        expect(this.messageError).toHaveText(message);
    }

    async clickSendPassword(): Promise<void> {
        (await this.sendPasswordButton).click();
    }

    passwordInputIsDisplayed(): void {
        expect(this.passwordInput).toBeDisplayed();
    }

    async setPassword(password: string): Promise<void> {
        (await this.passwordInput).setValue(password);
    }

    async clickLogIn(): Promise<void> {
        (await this.logInButton).click();
    }
}

export default AuthModal;