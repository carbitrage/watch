declare namespace WebdriverIO {
    interface Browser {
        pressTab: () => void
    }
}