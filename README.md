# Watch

Watch is testing framework for Cryptocurrency Arbitrage web application.

This project uses:
- **WebdriverIO** (browser actions automation)
- **Mocha** (testing framework)
- **Allure** (reporting tool)

## Installation
Clone current repository and install required packages via `npm install`. After that, configure environment variables and then execute `npm start`.

To generate report after tests run, execute `npm run report`.

Report may be served on local web server using command `npx allure open` in project's home directory or in report's directory. You may use this command without word `npx` as well if you have **Allure** installed globally.

## Configuration
Make a copy of `.env.example` file and rename it to `.env` and then open it.

### Environment variables
- **USER_LOGIN** - User ID of test user.
- **USER_PASSWORD** - password for test user.